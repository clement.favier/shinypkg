
<!-- README.md is generated from README.Rmd. Please edit that file -->

## Shinypkg

<!-- badges: start -->

<!-- badges: end -->

L’objectif de ce tutorial est d’illustrer l’utilité d’un code R packagé
et de Git.

## Objectifs d’apprentissage

  - Base de Git : fork, clone, add, commit, push.
  - Utilisation de git : en ligne de commande depuis le terminal.
  - Base d’un package R shiny

## Travailler sur un projet existant

Ce projet s’appuie sur le matériel disponible en :
<https://gitlab.com/cbo14142/shinypkg>

### 0\) Si besoin :

  - Télécharger et installer Git : <https://git-scm.com/downloads>

  - Configurer RStudio pour utiliser Git, dans RStudio \> Tools \>
    Global Options \> Git/SVN. Si le chemin vers l’exécutable `git.exe`
    n’est pas renseigné vérifier qu’il est présent en “C:/Program
    Files/Git/bin/git.exe” et l’ajouter.

  - Configurer git, dans le terminal de RStudio taper et compléter les
    commandes à suivre :

<!-- end list -->

``` r
git config --global user.name 'Your Name'
git config --global user.email 'your@email.com'
```

  - Créer un compte sur Gitlab (<https://gitlab.com/>)

### 1\) Forker (créer une fourche) le projet

Utiliser le projet de base (<https://gitlab.com/cbo14142/shinypkg>).

> Cela va importer le projet (dossiers, scripts, …) dans votre espace
> personnel

### 2\) Cloner le projet (depuis votre espace)

> Permet d’importer le projet sur son PC local.

  - 2.1 Dans les options de **Clone** (en bleu) copier *l’adresse http
    du répertoire projet*.
  - 2.2 Puis dans RStudio, placez vous dans l’arborescence voulu (e.g :
    “\~/Document/Gitlab\_training”).
  - 2.3 Entrez dans le terminal de RStudio la commande `git clone` puis
    collez *l’adresse http du répertoire projet* et exécutez cette ligne
    (en tapant sur `Entrer`) comme ci-dessous :

<!-- end list -->

``` r
git clone https://gitlab.com/cbo14142/myawesomeproject.git
```

2.4 Taper `git branch -a` pour visualiser les différentes branches
locales et distantes.

### 3\) Collaborer au projet

L’objectif est de créer une application de base avec 3 scripts : `ui.R`,
`server.R` et `run_app.R`.

**Application backlog**

Cette application s’appuiera sur le jeux de donnée `iris` et
représentera un simple scatterplot avec les espèces comme variable de
coloration et en dessous un tableau de statistiques descriptives (min,
médiane, moyenne, max). L’utilisateur aura le choix des variables à
représenter en abscisse et ordonné.

  - 3.1 Créer l’UI `ui.R` avec la commande `usethis::use_r("ui.R")`.
    Toutes les fonctions intervenant dans le package seront stockées
    dans le dossier `R`.

  - 3.2 Compléter le script selon les indications ci-dessus.

  - 3.3.1 Vérifier à enregistrer dans le répertoire git avec `git
    status`

  - 3.3.2 Ajouter ce script au répertoire git avec les commandes :

<!-- end list -->

``` r
git add ui.R
git commit -m "UI ajouté au projet"
```

> “git add” indique à git qu’il y a un nouveau fichier (ou un changement
> dans un fichier) à prendre en compte “git commit” écrit dans le
> répertoire git les changements dans le(s) fichier(s) concerné(s) “git
> commit -m” prends pour option “-m” = “message”

> En pratique il est recommendé d’effectuer des commits réguliers avec
> des messages clairs et concis expliquant pourquoi le changement a été
> effectué

  - 3.3.3 Vérifier qu’il n’y a pas de nouveau changement à commiter

  - 3.5 Ecrirer une fonction `stat_fun` calculant les statiques
    descriptives dans `stat_fun.R` avec les étapes git précédentes

  - 3.5 Reprendre les étapes précédentes pour créer `server.R`

  - 3.6 Reprendre les étapes précédentes pour créer `run_app.R`

Cette fonction doit permettre de lancer l’application avec :

``` r
run_app()
```

  - 3.7 Pousser (push) les changements locaux vers le control de version
    distant :

<!-- end list -->

``` r
git push
```

### 4\) R Package

Un package permet de partager un ensemble de fonctions documentées, de
données, … en s’assurant que les librairies requises sont installées.

  - 4.1 Cliquez sur `shinypkg.Rproj`, dans la partie `Build Tools`
    assurez vous que l’onglet “Generate documentation with Roxygen” est
    cochée avec l’option (dans `Configure...`) `Build and restart`

  - 4.2 Dans l’onglet `Build` (au niveau de `Environment`, `History`,
    `Connections`) cliquez sur `Install and Restart` (ou `ctrl + shift +
    B`). Et voilà \!

  - 4.3.1 “Tester” la bonne installation du package en redémarrant la
    session R (Session \> Restart … ou `ctrl + F10`) et en exécutant :

<!-- end list -->

``` r
library(shinypkg)
run_app()

# OU
shinypkg::run_app()
```

  - 4.3.2 Vérifier la qualité du package en effectuant un “Check” (dans
    `Build`, à côté de `Install and Restart` ou avec `ctrl + shift + E`)

> L’objectif est d’obtenir un check parfait du package : 0 error, 0
> warning, 0 notes Cette étape est cruciale pour éviter les mauvaises
> supprises par la suite (erreur dans la dockerisation, dans
> l’intégration et le déploiement)

  - 4.3.3 Résoudre les messages du `check` :
    
      - 4.3.3.1 Pour le `warning` concernant la license, utiliser
        `usethis::use_mit_license`
      - 4.3.3.2 Pour les `note` concernant les fichiers non utiles à la
        compilation du package, ajouter ces fichiers au fichier
        `.Rbuildignore` avec une commande du type
        `usethis::use_build_ignore(files = "file_name")`.
      - 4.3.3.3 Résoudre tous les (éventuels) autres problèmes liés aux
        scripts R

  - 4.4.1 Documentez le code. Dans les scripts, placez vous dans les
    fonctions et insérez un squelette de documentation (dans Code \>
    Insert Roxygen Squeletton ou `ctrl + shift + alt + R`). Complétez
    les champs requis.

  - 4.4.2 Re-builder le package pour tester la documentation avec :

<!-- end list -->

``` r
?stat_fun()
```

Optionnel : ajouter un exemple à la documentation de `stat_fun`

  - 4.4.3 Commiter les changements

  - 4.5 Insérer un test pour le lancement de l’application avec :

<!-- end list -->

``` r
usethis::use_test(name = "test-run-app")
```

en y insérant le test :

``` r
testthat::expect_error(object = shinypkg::run_app(),
                       regexp = NA
                       )
```

Cette fonction, comme indiqué dans `?testthat::expect_error`, test
l’absence d’erreur dans le lancement de l’application.

Tester l’application dans Build \> More \> Test Package (ou `ctrl + alt
+ T`)

  - 4.6 Créer un test pour `stat_fun`

## Conclusions

Dans ce projet nous avons construit un package R versionné sur un
répertoire git local et distant avec Gitlab. Ce package peut être plus
à même d’être un travail collaboratif avec sa documentation (pour le
futur nous ou un collègue) et ses tests (qui détectent des changements
qui cassent le code).

NB : Nous avons utilisé git en ligne de commande car les alternatives ne
permettent pas toujours d’utiliser toutes les commandes git (fusion de
projet notamment). Il y a au moins 3 autre alternatives :

  - Ecrire script R d’historique de ses commandes avec la fonction
    `system` qui permet de passer des commandes depuis R au terminal.
    E.g : `system("git status")`.

  - Utiliser le package `git2r`. Plus d’informations :
    <https://docs.ropensci.org/git2r/> .

  - Utiliser le panel `Git`
