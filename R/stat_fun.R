stat_fun = function(data){
	recap = data %>%
		group_by(Species) %>%
		summarise_all(.funs = mean)
	return(recap)
}
